#include "libxildrv.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#define MAX_HANDLES		32
#define MAX_PATH		256
#define BAR_SIZE		0x1000000 // 16 MB

struct xilinx_device_struct
{
	int user_fd;
	uint32_t *registers;
	uint32_t reg_nbr;
	char base_path[MAX_PATH];
	int h2c_fd[MAX_HANDLES];
	int c2h_fd[MAX_HANDLES];
};


xildev * xil_open_device(const char *device_base_name)
{
	xildev *dev;
	int user_fd;
	char full_path[MAX_PATH];
	if(!device_base_name)
		return NULL;

	snprintf(full_path, sizeof(full_path), "%s_user", device_base_name);

	user_fd = open(full_path, O_RDWR);
	if(user_fd == -1)
	{
		errno = EINVAL;
		return NULL;
	}

	dev = calloc(1, sizeof(*dev));
	if(!dev)
	{
		errno = ENOMEM;
		return NULL;
	}
	dev->user_fd = user_fd;
	strncpy(dev->base_path, device_base_name, MAX_PATH);
	dev->base_path[MAX_PATH-1] = '\0';

	dev->reg_nbr = BAR_SIZE / sizeof(*dev->registers);
	dev->registers = (uint32_t *)mmap(NULL, BAR_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, user_fd, 0);
	if (dev->registers == MAP_FAILED) {
		perror("Failed to map register memory:");
		dev->registers = NULL;
	}

	return dev;
}

int xil_close_device(xildev *dev)
{
	if(!dev)
	{
		errno = EINVAL;
		return -1;
	}
	if(dev->registers)
		munmap(dev->registers, BAR_SIZE);
	close(dev->user_fd);
	for(int i = 0; i < MAX_HANDLES; i++)
	{
		if(dev->h2c_fd[i])
			close(dev->h2c_fd[i]);
		if(dev->c2h_fd[i])
			close(dev->c2h_fd[i]);
	}
	free(dev);
	return 0;
}

unsigned int xil_get_bar_size(xildev *dev)
{
	if(!dev)
	{
		errno = EINVAL;
		return -1;
	}
	return dev->reg_nbr * sizeof(*dev->registers);
}

int xil_get_c2h_fd(xildev *dev, unsigned int channel)
{
	if(!dev)
	{
		errno = EINVAL;
		return -1;
	}
	if(channel >= MAX_HANDLES)
	{
		errno = EINVAL;
		return -1;
	}
	int fd = dev->c2h_fd[channel];
	if(fd)
		return fd;
	char full_path[MAX_PATH];
	if(snprintf(full_path, sizeof(full_path), "%s_c2h_%d", dev->base_path, channel) < 0)
	{
		errno = EINVAL;
		return -1;
	}
	fd = open(full_path, O_RDWR | O_NONBLOCK);
	if(fd >= 0)
		dev->c2h_fd[channel] = fd;
	return fd;
}

int xil_get_h2c_fd(xildev *dev, unsigned int channel)
{
	errno = EINVAL;
	if(!dev)
	{
		errno = EINVAL;
		return -1;
	}
	if(channel >= MAX_HANDLES)
	{
		errno = EINVAL;
		return -1;
	}
	int fd = dev->h2c_fd[channel];
	if(fd)
		return fd;
	char full_path[MAX_PATH];
	if(snprintf(full_path, sizeof(full_path), "%s_h2c_%d", dev->base_path, channel) < 0)
	{
		errno = EINVAL;
		return -1;
	}
	fd = open(full_path, O_RDWR | O_NONBLOCK);
	if(fd >= 0)
		dev->h2c_fd[channel] = fd;
	return fd;
}

int xil_read_reg(xildev *dev, uint32_t address, uint32_t *value)
{
	if(!dev || !value)
	{
		errno = EINVAL;
		return -1;
	}
	if(address & 3)
	{
		errno = EINVAL;
		return -1;
	}
	if(dev->registers)
	{
		uint32_t reg_no = address / 4;
		if(reg_no < dev->reg_nbr)
		{
			*value = dev->registers[reg_no];
			return 0;
		}
	}
	else
	{
		ssize_t sz = sizeof(*value);
		ssize_t ret = pread(dev->user_fd, value, sz, address);
		if(ret == sz) return 0;
	}
	errno = EINVAL;
	return -1;
}

int xil_write_reg(xildev *dev, uint32_t address, uint32_t value)
{
	if(!dev)
	{
		errno = EINVAL;
		return -1;
	}
	if(address & 3)
	{
		errno = EINVAL;
		return -1;
	}
	if(dev->registers)
	{
		uint32_t reg_no = address / 4;
		if(reg_no < dev->reg_nbr)
		{
			dev->registers[reg_no] = value;
			return 0;
		}
	}
	else
	{
		ssize_t sz = sizeof(value);
		ssize_t ret = pwrite(dev->user_fd, &value, sz, address);
		if(ret == sz) return 0;
	}
	errno = EINVAL;
	return -1;
}
