/**
 * @file evrSnifferPortDriver.cpp
 * @author Joao Paulo Martins (joaopaulo.martins@ess.eu)
 * @brief asynPortDriver class
 * @version 0.1
 * @date 2022-09-15
 * 
 * @copyright Copyright (c) ESS ERIC 2022
 * 
 */

#include <memory>
#include <iostream>
#include <stdexcept>
#include <ctime>
#include <string>
#include <vector>
#include <thread>
#include <cstddef>
#include <chrono>
#include <csignal>

#include <epicsThread.h>
#include <iocsh.h>
#include "evrSnifferPortDriver.h"
#include <epicsExport.h>
#include <epicsExit.h>

// unique_ptr that holds the instance of the driver created
std::shared_ptr<evrSnifferPortDriver> evrSniffer;

// other variables
static std::string driverName("evrSnifferPortDriver");

// void exitIOCHandler(int signum)
// {
// 	std::cout << "\n[FIMIOC] Interrupt signal captured! " << std::endl;
// 	exit(signum);
// }

// Handler for polling thread
void pollTaskC(void *drvPvt)
{
    evrSnifferPortDriver *pPvt = (evrSnifferPortDriver *)drvPvt;
    pPvt->pollTask();
}

// Port Driver Constructor
evrSnifferPortDriver::evrSnifferPortDriver(const std::string &drvPortName, const std::string &devName)
    : asynPortDriver(drvPortName.c_str(), 
                     MaxAddr, 
                     InterfaceMask, 
                     InterruptMask,
                     AsynFlags, AutoConnect, Priority, StackSize)
{
    asynStatus status;
    static std::string functionName("evrSnifferPortDriver"); 

    // Create asyn Params
    createParam(P_EnableStr,              asynParamInt32,         &P_Enable);
    createParam(P_TX17CntStr,             asynParamInt32,         &P_TX17Cnt);
    createParam(P_TX18CntStr,             asynParamInt32,         &P_TX18Cnt);
    createParam(P_TX19CntStr,             asynParamInt32,         &P_TX19Cnt);
    createParam(P_TX20CntStr,             asynParamInt32,         &P_TX20Cnt);
    createParam(P_RX17CntStr,             asynParamInt32,         &P_RX17Cnt);
    createParam(P_RX18CntStr,             asynParamInt32,         &P_RX18Cnt);
    createParam(P_RX19CntStr,             asynParamInt32,         &P_RX19Cnt);
    createParam(P_RX20CntStr,             asynParamInt32,         &P_RX20Cnt);
    createParam(P_PollPeriodStr,          asynParamFloat64,       &P_PollPeriod);

    //Set initial values
    setIntegerParam(P_Enable, 0);
    setIntegerParam(P_TX17Cnt, 0);
    setIntegerParam(P_TX18Cnt, 0);
    setIntegerParam(P_TX19Cnt, 0);
    setIntegerParam(P_TX20Cnt, 0);
    setIntegerParam(P_RX17Cnt, 0);
    setIntegerParam(P_RX18Cnt, 0);
    setIntegerParam(P_RX19Cnt, 0);
    setIntegerParam(P_RX20Cnt, 0);

    setDoubleParam(P_PollPeriod, 0.05);


	// // Register EXIT signal handler 
	// std::signal(SIGTERM, exitIOCHandler);
	// std::signal(SIGINT, exitIOCHandler);

    // tries to open XDMA device
    initDevice(devName);

    /* Create the polling thread */
    status = (asynStatus)(epicsThreadCreate("drvEVRSnifferThread",
                          epicsThreadPriorityMedium,
                          epicsThreadGetStackSize(epicsThreadStackMedium),
                          (EPICSTHREADFUNC)pollTaskC,
                          this) == NULL);
    if (status) {
        throw std::runtime_error("[IFC14x0 XDMA] - (evrSnifferPortDriver) epicsThreadCreate failure");
    }
}

// Class destructor
evrSnifferPortDriver::~evrSnifferPortDriver() {
    // close XDMA device
    if (m_xdmaDev) {
        xil_close_device(m_xdmaDev);
    }
}

void evrSnifferPortDriver::initDevice(const std::string &devName) {
    m_xdmaDev = xil_open_device(devName.c_str());
    if (!m_xdmaDev) {
        throw std::runtime_error("[IFC14x0 XDMA] - ERROR - can't open " + devName);
    }

    std::cout << "[IFC14x0 XDMA] Succesfully opened XDMA device " << devName << std::endl;

    // Reset counters
    xil_write_reg(m_xdmaDev, IFC14x0_CTL_REG, 0);
    xil_write_reg(m_xdmaDev, IFC14x0_CTL_REG, 1);
    setIntegerParam(P_Enable, 1);

}

void evrSnifferPortDriver::pollTask() {

    while (1) {
        int status = 0;
        uint32_t val[8];

        lock();
        
        // Read backplane line counter registers
        status |= xil_read_reg(m_xdmaDev, IFC14x0_TX17_REG, &val[0]); 
        status |= xil_read_reg(m_xdmaDev, IFC14x0_TX18_REG, &val[1]); 
        status |= xil_read_reg(m_xdmaDev, IFC14x0_TX19_REG, &val[2]); 
        status |= xil_read_reg(m_xdmaDev, IFC14x0_TX20_REG, &val[3]); 
        status |= xil_read_reg(m_xdmaDev, IFC14x0_RX17_REG, &val[4]); 
        status |= xil_read_reg(m_xdmaDev, IFC14x0_RX18_REG, &val[5]); 
        status |= xil_read_reg(m_xdmaDev, IFC14x0_RX19_REG, &val[6]); 
        status |= xil_read_reg(m_xdmaDev, IFC14x0_RX20_REG, &val[7]);

        if (status) {
            std::cout << "Error reading EVR Sniffer registers" << std::endl;
        } else {
            setIntegerParam(P_TX17Cnt, (int)val[0]);           
            setIntegerParam(P_TX18Cnt, (int)val[1]);
            setIntegerParam(P_TX19Cnt, (int)val[2]);
            setIntegerParam(P_TX20Cnt, (int)val[3]);
            setIntegerParam(P_RX17Cnt, (int)val[4]);
            setIntegerParam(P_RX18Cnt, (int)val[5]);
            setIntegerParam(P_RX19Cnt, (int)val[6]);
            setIntegerParam(P_RX20Cnt, (int)val[7]);
        }

        // Set parameters
        callParamCallbacks();
        double wait_period;
        getDoubleParam(P_PollPeriod, &wait_period);

        unlock();
        epicsThreadSleep(wait_period);
    }
}

asynStatus evrSnifferPortDriver::writeInt32(asynUser *pasynUser, epicsInt32 value) {
    int addr;
    int function = pasynUser->reason;
    int status = 0;
    static const char *functionName = "writeInt32";
    
    this->getAddress(pasynUser, &addr);
    setIntegerParam(function, value);

    // Analog input functions
    if (function == P_Enable) {
        status = xil_write_reg(m_xdmaDev, IFC14x0_CTL_REG, (uint32_t)value);
    }
  
    callParamCallbacks();
    if (status == 0) {
      asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
               "%s:%s, port %s, wrote %d to address %d\n",
               driverName.c_str(), functionName, this->portName, value, addr);
    } else {
      asynPrint(pasynUser, ASYN_TRACE_ERROR,
               "%s:%s, port %s, function=%d, ERROR writing %d to address %d, status=%d\n",
               driverName.c_str(), functionName, this->portName, function, value, addr, status);
    }
    return (status==0) ? asynSuccess : asynError;
}

asynStatus evrSnifferPortDriver::writeFloat64(asynUser *pasynUser, epicsFloat64 value) {
    int addr;
    int function = pasynUser->reason;
    int status=0;
    static const char *functionName = "writeFloat64";
    
    this->getAddress(pasynUser, &addr);

    // Analog input functions
    if (function == P_PollPeriod) {
        if (value < 0.050) value = 0.050;
        setDoubleParam(function, value);
    }

    callParamCallbacks();
    if (status == 0) {
      asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
               "%s:%s, port %s, wrote %f to address %d\n",
               driverName.c_str(), functionName, this->portName, value, addr);
    } else {
      asynPrint(pasynUser, ASYN_TRACE_ERROR,
               "%s:%s, port %s, ERROR writing %f to address %d, status=%d\n",
               driverName.c_str(), functionName, this->portName, value, addr, status);
    }
    return (status==0) ? asynSuccess : asynError;
}


//-----------------------------------------------------------------------------
// Driver instantiation
//-----------------------------------------------------------------------------
extern "C" {

// TODO: proper clean-up before exit
static void drvEVRSniffer_cleanUp(void *)
{
    //std::cout << "Exiting......" << std::endl;
}

// Create driver instance
int drvEVRSnifferConfig(char *drvPortName, char *devPortName)
{
    try {
        std::string portName = std::string(drvPortName);
        std::string devName = std::string(devPortName);

        evrSniffer = std::make_shared<evrSnifferPortDriver>(portName, devName);
    } catch (const std::exception& e) {
        std::cerr << '\n' << "[ERROR] Port: " << drvPortName << " ; " << typeid(e).name()  << " ; " << e.what() << std::endl;
        exit(-1);
    }
    
    return 0;
}

//-----------------------------------------------------------------------------
// EPICS iocsh shell commands
//-----------------------------------------------------------------------------

// IOC-shell command "drvEVRSnifferConfig"
static const iocshArg config_Arg0 { "drvPortName", iocshArgString };
static const iocshArg config_Arg1 { "devPortName", iocshArgString };

static const iocshArg * const config_Args[] {
  &config_Arg0,
  &config_Arg1
};

static const iocshFuncDef config_FuncDef {
  "drvEVRSnifferConfig",
  sizeof(config_Args) / sizeof(iocshArg *),
  config_Args
};

static void config_CallFunc(const iocshArgBuf *args)
{
  drvEVRSnifferConfig(args[0].sval, args[1].sval);
}

/**
 * @brief The function that registers the EPIS IOC Shell functions
 */
void drvEVRSniffer_Register(void)
{
  static bool firstTime = true;

  if (firstTime)
  {
    epicsAtExit(drvEVRSniffer_cleanUp, nullptr);
    iocshRegister(&config_FuncDef, config_CallFunc);
    firstTime = false;
  }
}

epicsExportRegistrar(drvEVRSniffer_Register);

}  // extern "C"

//-----------------------------------------------------------------------------

